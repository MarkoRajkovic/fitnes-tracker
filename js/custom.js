(function ($) {
    fetch('https://api.myjson.com/bins/1gwnal')
        .then(response => {
            return response.json()
        })
        .then(data => {

            /* variables */
            var n = data.length;
            var total_steps = 0;//total steps
            var days = [];//days list
            var days_single = [];//single day info
            var days_total = [];//unique days
            var days_steps = '';//days and steps from api
            var i = '';//variable for for loop
            var total_steps_by_day = {}; //sum by day
            var days_name = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];//days names in week
            var month_names = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];//month names in year


            for (i = 0; i < n; i++) {
                days_total.push(convert(new Date(data[i].timestamp)));
                total_steps += data[i].steps;
                days_steps += (' / ' + (convert(new Date(data[i].timestamp))) + ' ' + data[i].steps);
            }
            days_total = arrUnique(days_total);//unique days


            var all_days_steps = days_steps.split('/');//days and steps from api splited
            for (i = 0; i < all_days_steps.length; i++) {
                var separate_days_steps = all_days_steps[i].split(' ');//separate days and steps
                var date_single = separate_days_steps[1].split('-');//only date from api
                var sum = 0;
                /* Sum of steps by one day */
                if (total_steps_by_day.hasOwnProperty(date_single[2])) {
                    sum = total_steps_by_day[date_single[2]];
                }
                sum += parseFloat(separate_days_steps[2]);
                total_steps_by_day[date_single[2]] = sum;
            }


            for (i = 0; i < days_total.length; i++) {
                /* single days list */
                days.push(
                    '<div id=js-day-' + i + ' class="one_day">' +
                    '<span>' +
                    '<h5>' + days_total[i].split("-")[2] + '</h5>' +
                    '<h5>' + days_name[new Date(days_total[i]).getDay()].substring(0, 3) + '</h5>' +
                    '</span>' +
                    '</div>');


                /* single day info */
                days_single.push(
                    '<div id=js-day-' + i + ' class="one_day_info">' +
                    '<div class="day_single">' +
                    '<img src="./img/icons/baseline-chevron_left-24px.svg"/>' +
                    '<h1>' + days_name[new Date(days_total[i]).getDay()] + '<span>' + month_names[new Date(days_total[i]).getMonth()] + ' ' + days_total[i].split("-")[2] + ', ' + days_total[i].split("-")[0] + '.</span>' + '</h1 > ' +
                    '</div>' +
                    '<div class="days_div js-days-single"></div>' +
                    '<div class="day_info">' +
                    '<div class="circle">' +
                    '<div class="circle_info">' +
                    '<div class="inline_block">' +
                    '<div class="tracker_info_dot"><img src="./img/icons/baseline-directions_run-24px.svg"/></div>' +
                    '</div>' +
                    '<span>Steps</span><h1>' + formatNumberComma(total_steps_by_day[days_total[i].split("-")[2]]) + '</h1>' +
                    '</div>' +
                    '</div>' +
                    '<div class="message_info">' +
                    '<span>Very good</span>' +
                    '<h1> Keep going!</h1>' +
                    '</div>' +
                    '<div class="bottom_details">' +
                    '<div class="bottom_info">' +
                    '<span>km</span>' +
                    '<h1>' + (total_steps_by_day[days_total[i].split("-")[2]] * 0.762 * 0.001).toFixed(1) + '</h1>' +
                    '</div>' +
                    '<div class="bottom_info">' +
                    '<span>cal</span>' +
                    '<h1>' + (total_steps_by_day[days_total[i].split("-")[2]] * 0.05).toFixed(0) + '</h1>' +
                    '</div>' +
                    '<div class="bottom_info">' +
                    '<span>hours</span>' +
                    '<h1>' + ((total_steps_by_day[days_total[i].split("-")[2]] * 0.5) / 3600).toFixed(1) + '</h1>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
                );
            }


            /* Append data on pages */
            var total_calories = (total_steps * 0.05).toFixed(0);//total calories
            var total_time = getTime(((total_steps * 0.5).toFixed(2)) / days_total.length);//total time
            $('.container').append(days_single);//append days single with info
            $('.js-days').append(days);//append days list on home page
            $('.js-days-single').append(days);//append days list on single page
            $('.js-total-steps').append(formatNumberDot(total_steps));//total steps home page
            $('.js-total-calories').append(parseInt(total_calories));//total calories home page
            $('.js-average-time').append(total_time);//average time home page
        });


    /* FUNCTIONS */

    /* Format time into days hours minutes and seconds */
    function getTime(seconds) {
        var leftover = seconds;
        var days = Math.floor(leftover / 86400);
        leftover = leftover - (days * 86400);
        var hours = Math.floor(leftover / 3600);
        leftover = leftover - (hours * 3600);
        var minutes = Math.floor(leftover / 60);
        leftover = leftover - (minutes * 60);
        return ((days > 0 ? days + 'd ' : '') + (hours > 0 ? hours + 'h ' : '') + minutes + 'min');
    }

    /* Format number with dot */
    function formatNumberDot(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    /* Format number with comma */
    function formatNumberComma(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    /* Convert timestamp in Y-M-D */
    function convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }

    /* Remove unique elements */
    function arrUnique(a) {
        var t = [];
        for (var x = 0; x < a.length; x++) {
            if (t.indexOf(a[x]) == -1) t.push(a[x]);
        }
        return t;
    }

    /* Display single day */
    $(document).on("click", ".js-days .one_day", function () {
        var obj = $(this);
        var id = obj.attr('id');
        $('.one_day_info').removeClass('active , active_single');
        $('.js-days-single .one_day').removeClass('active');
        $('.one_day_info#' + id).addClass('active');
        $('.js-days-single #' + id).addClass('active');
    });
    $(document).on("click", ".js-days-single .one_day", function () {
        var obj = $(this);
        var id = obj.attr('id');
        $('.one_day_info').removeClass('active , active_single');
        $('.js-days-single .one_day').removeClass('active');
        $('.one_day_info#' + id).addClass('active_single');
        $('.js-days-single #' + id).addClass('active');
    });
    $(document).on("click", ".day_single img", function () {
        $('.one_day_info').removeClass('active , active_single');
    });

})(jQuery);